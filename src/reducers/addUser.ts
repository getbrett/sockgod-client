import UserMessage, { IMessage } from '../../models/Message';
import { Action } from '../actions';
import { IChatState } from '../state';

const initalState: IChatState = {
    messages: [],
    users: [],
};

export function addUser(state: IChatState = initalState, action: Action): IChatState {
    switch (action.type) {
        case 'CHAT/ADD_USER':
            const joiningUserMessageObj: IMessage = {
                message: 'joined the game',
                name: action.username,
            };

            const joiningUserMessage: IMessage = new UserMessage(JSON.stringify(joiningUserMessageObj));
            action.socket.send(JSON.stringify(joiningUserMessage));

            return {
                messages: state.messages,
                users: [...state.users, action.username],
            };

            default:
                return state;
    }
}
