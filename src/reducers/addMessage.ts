import { Action } from '../actions';
import { IChatState } from '../state';

const initalState: IChatState = {
    messages: [],
    users: [],
};

export function addMessage(state: IChatState = initalState, action: Action): IChatState {
    switch (action.type) {
        case 'CHAT/ADD_MESSAGE':
            return {
                messages: [...state.messages, action.message],
                users: state.users,
            };

            default:
                return state;
    }
}
