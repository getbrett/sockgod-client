import { IMessage } from '../../models/Message';

export type Action = {
    message: IMessage,
    type: 'CHAT/ADD_MESSAGE',
} | {
    socket: WebSocket,
    type: 'CHAT/ADD_USER',
    username: string,
};

export const addMessageAction = (message: IMessage): Action => ({
    message,
    type: 'CHAT/ADD_MESSAGE',
});

export const addUserAction = (username: string, socket: WebSocket): Action => ({
    socket,
    type: 'CHAT/ADD_USER',
    username,
});
