import * as React from 'react';
import { connect } from 'react-redux';
import * as redux from 'redux';

import { Action, addUserAction } from '../../actions';
import { IChatState } from '../../state';

import { ChatApp } from '../ChatApp/ChatApp';

const mapStateToProps = (state: IChatState, ownProps: IOwnProps): {} => ({});

const mapDispatchToProps = (dispatch: redux.Dispatch<Action>): IConnectedDispatch => ({
    addUser: (username: string, socket: WebSocket) => {
      dispatch(addUserAction(username, socket));
    },
  });

interface IOwnProps {
    socket: WebSocket;
}

interface IConnectedDispatch {
    addUser: (username: string, socket: WebSocket) => void;
}

interface IOwnState {
    username: string;
    submitted: boolean;
}

export class AppComponent extends React.Component<IConnectedDispatch & IOwnProps, IOwnState> {

    public state = {
        submitted: false,
        username: '',
    };

    public usernameChangeHandler = (event: any) => {
        this.setState({ username: event.target.value });
    }

    public usernameSubmitHandler = (event: any) => {
        event.preventDefault();
        this.setState({ submitted: true, username: this.state.username });
        this.props.addUser(this.state.username, this.props.socket);
    }

    public render() {
        if (this.state.submitted) {
            // Form was submitted, now show the main App
            return (
                <ChatApp username={this.state.username} socket={this.props.socket} />
            );
        }

        return (
            <form onSubmit={this.usernameSubmitHandler} className='username-container'>
                <h1>React Instant Chat</h1>
                <div>
                    <input
                        type='text'
                        onChange={this.usernameChangeHandler}
                        placeholder='Enter a username...'
                        required={true}
                    />
                </div>
                <input type='submit' value='Submit' />
            </form>
        );
        }
  }

export const App: React.ComponentClass<IOwnProps> = connect(mapStateToProps, mapDispatchToProps)(AppComponent);
