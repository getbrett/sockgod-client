import { IMessage } from '../models/Message';

export interface IChatState {
    messages: IMessage[];
    users: string[];
}
