module.exports = {
    devtool: 'source-map',
    entry: './src/index.ts',
    output: {
      filename: './build/server.js'
    },
    resolve: {
      extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
      loaders: [{ test: /\.tsx?$/, loader: 'ts-loader' }]
    }
};
